import sbt.Keys._
import sbtbuildinfo.BuildInfoPlugin.autoImport._

object BuildInfo {

  val settings = Seq(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "workout.log",
    buildInfoUsePackageAsPath := true,
    buildInfoOptions += BuildInfoOption.ToJson
  )
}