import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import com.typesafe.sbt.packager.docker._

object Packaging {

  val settings = Seq(
    dockerBaseImage := "anapsix/alpine-java:8",
    dockerRepository := Some("argast"),
    dockerBuildOptions ++= Seq("-t", dockerAlias.value.copy(tag = Some("local")).toString),
    dockerCommands += Cmd("HEALTHCHECK", "--interval=5s", "--timeout=1s", "CMD", "wget", "--spider", "-q", "http://localhost:8080/health")
  )
}