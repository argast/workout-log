import sbt.Keys._
import sbt._
import _root_.io.gatling.sbt.GatlingKeys

object Gatling {

  val settings = Seq(
    resourceDirectory in GatlingKeys.Gatling := sourceDirectory.value / "gatling" / "resources",
    scalaSource in GatlingKeys.Gatling := sourceDirectory.value / "gatling" / "scala",
    fullClasspath in GatlingKeys.Gatling += crossTarget.value / "gatling-classes"
  )
}
