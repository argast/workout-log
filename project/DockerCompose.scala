import java.net.{URI => _, _}

import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import org.scalatest.concurrent.{Eventually, PatienceConfiguration}
import sbt.Keys._
import sbt._

import scala.collection.convert.DecorateAsScala
import scala.concurrent.duration._

object DockerCompose extends DecorateAsScala {

  def getLocalIpAddress = {
    val ip4Address: PartialFunction[InetAddress, String] = { case a: Inet4Address => a.getHostAddress }
    val interfaces = NetworkInterface.getNetworkInterfaces.asScala.toSeq
    val interface = interfaces.find(i => i.getName == "en0" || i.getName == "eth0")
    interface.flatMap(_.getInetAddresses.asScala.toSeq.collect(ip4Address).headOption).getOrElse(InetAddress.getLocalHost.getHostAddress)
  }

  def dockerMachineIp =
    Option(System.getenv("DOCKER_HOST")).map(h => new URI(h).getHost) // docker-machine setup
      .getOrElse(getLocalIpAddress) // docker for mac setup

  private val dockerComposeUpTask: Def.Initialize[Task[Unit]] = Def.task {
    val log = streams.value.log
    (publishLocal in Docker).value
    scala.sys.process.Process(Seq("docker-compose", "up", "-d"), None, "VERSION" -> "local").!!
    Eventually.eventually(PatienceConfiguration.Timeout(20 seconds), PatienceConfiguration.Interval(1 second)) {
      log.info(s"Waiting for service to be ready on $dockerMachineIp:8080.")
      val c = new URL(s"http://$dockerMachineIp:8080/health").openConnection().asInstanceOf[HttpURLConnection]
      assert(c.getResponseCode == 200)
    }
    log.info(s"Service ready.")
  }

  private val dockerComposeDownTask: Def.Initialize[Task[Unit]] = Def.task {
    scala.sys.process.Process(Seq("docker-compose", "down"), None, "VERSION" -> "local").!!
  }

  val dockerComposeUp: TaskKey[Unit] = taskKey("Start docker compose")
  val dockerComposeDown: TaskKey[Unit] = taskKey("Stop docker compose")

  val settings = Seq(
    dockerComposeUp := dockerComposeUpTask.value,
    dockerComposeDown := dockerComposeDownTask.value
  )
}