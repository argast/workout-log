import sbt._
import sbt.Keys._

object Testing {

  def isLocalTest = System.getProperty("test.against", "local") == "local"

  val settings = Defaults.itSettings ++ Seq(
    Test / testOptions += Tests.Argument(TestFrameworks.ScalaTest, "-C", "reporter.TestReporter")
  )
}