import com.typesafe.sbt.GitPlugin.autoImport._

object Versioning {

  val settings = Seq(
    git.useGitDescribe := true
  )
}