import com.typesafe.config.ConfigFactory

object GatlingTestConfig {

  private lazy val config = ConfigFactory.load(System.getProperty("test.against", "local") + ".conf")

  lazy val hostname = config.getString("service.hostname")
}
