import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers, Tag}

class CucumberTest extends FeatureSpec with Matchers with GivenWhenThen with TableDrivenPropertyChecks {



  feature("Test feature") {
    info("As test")
    info("I want")
    info("To avoid")
    info("Writing cucumber")
    scenario("This is  a test scenario") {
      Given("this is given")
      And("that is given")
      When("I do that")
      Then("this happens")
    }


    forAll(Table(("a", "b"), ("c", "D"), ("e", "f"))) { (a, b) =>
      scenario(s"This is another test scenario $a, $b", Tag("MCC123456_100")) {

          Given(s"this is also given $a")
          And("that is given as well")
          When(s"I do something else $b")
          Then("something else happens")
        }

    }
  }

}
