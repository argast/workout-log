import cats.effect.IO
import org.http4s.{Method, Request, Status, Uri}
import org.scalatest.{Matchers, WordSpec}
import org.http4s.dsl.io._
import workout.log.BuildInfo

class InfoServiceTest extends WordSpec with Matchers {

  private val service = new InfoService[IO]().service.orNotFound

  "InfoService" should {
    "show info page" in {
      val response = service.run(Request(method = Method.GET, uri = Uri.uri("/info"))).unsafeRunSync()
      response.status shouldEqual Status.Ok
      response.as[String].unsafeRunSync() shouldEqual BuildInfo.toJson
    }
  }
}
