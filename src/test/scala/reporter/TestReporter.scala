package reporter

import org.scalatest.Reporter
import org.scalatest.events._

class TestReporter extends Reporter {
  override def apply(event: Event): Unit = event match {
    case ScopeOpened(_, message, _, _, _, _, _, _) =>
      println(message)

    case InfoProvided(_, message, _, _, _, _, _, _, _) =>
      println(message)
    case TestSucceeded(_, _, _, _, _, testText, recordedEvents, _, _, _, _, _, _, _) =>
      println(testText)
      recordedEvents.collect {
        case InfoProvided(_, message, _, _, _, _, _, _, _) => message
      }.foreach(println)
    case otherwise =>
  }
}
