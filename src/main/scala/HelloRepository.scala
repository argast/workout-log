import cats.effect.Effect
import cats.implicits._
import doobie.implicits._
import doobie.util.transactor.Transactor

import scala.language.higherKinds

trait HelloRepositoryAlg[F[_]] {
  def helloMessage: F[Option[String]]
}

class HelloRepository[F[_]](db: Transactor[F])(implicit F: Effect[F]) extends HelloRepositoryAlg[F] {
  override def helloMessage: F[Option[String]] =
    sql"select message from test".query[String].option.transact(db)
}