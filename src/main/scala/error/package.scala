package object error {
  case class AppError(code: String, message: String)

  abstract class AppException(val code: String, val message: String)
      extends Exception(message)

  abstract class ValidationError(code: String, message: String) extends AppException(code, message)

  case object NameEmpty
      extends ValidationError("NAME_EMPTY", "Name cannot be an empty string.")
  case object NameMissing
      extends ValidationError("NAME_MISSING", "Name parameter is required.")
}
