
case class Database(
                     url: String,
                     username: String,
                     password: String
                   )

case class Configuration(
                          port: Int,
                          database: Database
                        )
