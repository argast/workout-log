import cats.effect.Effect
import org.flywaydb.core.Flyway

class DbMigration {

  def migrate[F[_]](c: Configuration)(implicit F: Effect[F]): F[Unit] = {
    F.delay {
      val flyway = new Flyway()
      flyway.setDataSource(c.database.url,
        c.database.username,
        c.database.password)
      flyway.migrate()
    }
  }

}
