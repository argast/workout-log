import cats.effect.Effect
import com.typesafe.scalalogging.StrictLogging
import org.http4s.HttpService
import org.http4s.dsl.Http4sDsl
import workout.log.BuildInfo

import scala.language.higherKinds

class InfoService[F[_]: Effect]()
    extends Http4sDsl[F]
    with StrictLogging {

  val service: HttpService[F] = HttpService[F] {
    case GET -> Root / "info" => Ok(BuildInfo.toJson)
  }
}
