package workout.log

import cats.MonadError
import cats.data._
import cats.effect._
import error._

// Attempt to use an ADT as an error representation
//object Result {
//
//  type Result[F[_], T] = EitherT[F, AppError, T]
//
//  implicit def resultEffect[F[_]: Effect]
//    : Unexceptional[Result[F, ?], AppError] =
//    new Unexceptional[Result[F, ?], AppError] {
//
//
//      private val result1Async: Async[Result[F, ?]] =
//        Async.catsEitherTAsync[F, AppError]
//
//
//      override def bracketCase[A, B](acquire: Result[F, A])(use: A => Result[F, B])(release: (A, ExitCase[Throwable]) => Result[F, Unit]): Result[F, B] =
//        result1Async.bracketCase(acquire)(use)(release)
//
//      override protected def E: MonadError[Result[F, ?], AppError] =
//        implicitly[MonadError[Result[F, ?], AppError]]
//
//
//      override def runSyncStep[A](fa: Result[F, A]): IO[Either[Result[F, A], A]] = ???
//
//      override def asyncF[A](k: (Either[Throwable, A] => Unit) => Result[F, Unit]): Result[F, A] = result1Async.asyncF(k)
//
//      override def runAsync[A](fa: Result[F, A])(
//          cb: Either[Throwable, A] => IO[Unit]): IO[Unit] = {
//        val F = implicitly[Effect[F]]
//        F.runAsync(fa.value) {
//          case Right(a) =>
//            cb(a.left.map[Throwable](e => new Exception(e.toString)))
//          case Left(t) => IO.raiseError(t)
//        }
//      }
//
//      override def async[A](
//          k: (Either[Throwable, A] => Unit) => Unit): Result[F, A] =
//        result1Async.async(k)
//
//      override def suspend[A](thunk: => Result[F, A]): Result[F, A] =
//        result1Async.suspend(thunk)
//
//      override def flatMap[A, B](fa: Result[F, A])(
//          f: A => Result[F, B]): Result[F, B] =
//        result1Async.flatMap(fa)(f)
//
//      override def tailRecM[A, B](a: A)(
//          f: A => Result[F, Either[A, B]]): Result[F, B] =
//        result1Async.tailRecM(a)(f)
//
//      override def raiseError[A](e: Throwable): Result[F, A] =
//        result1Async.raiseError(e)
//
//      override def handleErrorWith[A](fa: Result[F, A])(
//          f: Throwable => Result[F, A]): Result[F, A] =
//        result1Async.handleErrorWith(fa)(f)
//
//      override def pure[A](x: A): Result[F, A] = result1Async.pure(x)
//    }
//
//  trait Unexceptional[F[_], E] extends Effect[F] {
//
//    protected def E: MonadError[F, E]
//
//    def raiseE[A](e: E): F[A] =
//      E.raiseError(e)
//
//    def handleEWith[A](fa: F[A])(f: E => F[A]): F[A] =
//      E.handleErrorWith(fa)(f)
//  }
//
//}

// Example how to use Unexceptional
//object TestResult extends App {
//
//  import cats._
//  import cats.data._
//  import cats.effect._
//  import cats.implicits._
//  import workout.log.Result._
//  import error.AppError
//
//  def test[F[_]](s: String)(
//      implicit F: Unexceptional[F, AppError]): F[String] = {
//
//    for {
//      a <- F.pure("a")
//      b <- F.pure("b")
//      _ <- if (s == "Exception") F.raiseError[String](new Exception("c"))
//      else F.raiseE[String](AppError("D", "d"))
//    } yield a + b
//  }
//
//  println(test[Result[IO, ?]]("ADT").value.unsafeRunSync())
//  println(test[Result[IO, ?]]("Exception").value.unsafeRunSync())
//}
