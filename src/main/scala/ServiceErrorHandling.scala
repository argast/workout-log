import cats.data._
import cats.effect.Effect
import error.{AppException, ValidationError}
import io.circe.syntax._
import io.circe.{Encoder, Json}
import org.http4s.HttpService
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

object ServiceErrorHandling {

  implicit class ErrorHandlingOps[F[_]](s: HttpService[F])(implicit F: Effect[F]) {

    private implicit def appExceptionEncoder[T <: AppException] = Encoder.instance[T] { e =>
      Json.obj(
        "code" -> Json.fromString(e.code),
        "message" -> Json.fromString(e.message)
      )
    }
    def withErrorHandling: HttpService[F] = {
      val dsl = Http4sDsl[F]
      import dsl._
      Kleisli { request =>
        OptionT {
          F.recoverWith(s(request).value) {
            case e: ValidationError => F.map(BadRequest(e.asJson))(Some(_))
            case e: AppException => F.map(UnprocessableEntity(e.asJson))(Some(_))
          }
        }
      }
    }
  }
}
