import cats.effect._
import kamon.Kamon
import kamon.http4s.Log
import cats.data._
import cats.implicits._
import error._

import scala.language.higherKinds

trait HelloAlg[F[_]] {
  def helloMessage: F[String]
  def helloMessage(name: Option[String]): F[String]
}

case class Hello[F[_]](helloRepository: HelloRepository[F])(implicit F: Effect[F], L: Log[F]) extends HelloAlg[F] {
  override def helloMessage: F[String] =
    for {
      _ <- L.info(
        "Hello service: " + Kamon.currentSpan().context().traceID.string)
      message <- F.pure(
        "Hello " + Kamon.currentSpan().context().traceID.string + "!!!")
    } yield message

  override def helloMessage(name: Option[String]): F[String] = {
    val message = for {
      helloMessage <- OptionT(helloRepository.helloMessage)
      name <- OptionT.liftF(validateName(name))
    } yield helloMessage.format(name)
    message.getOrElseF(F.raiseError(NameEmpty))
  }

  private def validateName(name: Option[String]): F[String] = {
    name.map { n =>
      if (n.isEmpty) F.raiseError[String](NameEmpty) else F.pure(n)
    }.getOrElse(F.raiseError[String](NameMissing))
  }

}
