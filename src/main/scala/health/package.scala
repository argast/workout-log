import cats.data.{Validated, ValidatedNel}

package object health {

  type HealthResult = Validated[String, Unit]

  trait HealthCheck {
    def check(): HealthResult
  }

  case class NamedHealthCheck(name: String, test: () => HealthResult) extends HealthCheck {
    override def check(): HealthResult = test()
  }
}

//object Test extends App {
//  import health._
//  import cats._
//  import cats.implicits._
//  import io.circe.generic.auto._
//  import io.circe.syntax._
//
//  val c1 = NamedHealthCheck("Check1", () => Validated.valid(()))
//  val c2 = NamedHealthCheck("Check2", () => Validated.invalid("B"))
//  val c3 = NamedHealthCheck("Check3", () => Validated.invalid("C"))
//
//  private val checks = List(c1, c2, c3)
//  println(checks.map(c => c.name -> c.check().fold(identity, _ => "OK")).toMap.asJson)
//}