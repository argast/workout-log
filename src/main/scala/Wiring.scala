import cats.effect.Effect
import cats.implicits._
import doobie.util.transactor.Transactor

class Wiring[F[_]](val configuration: Configuration)(implicit F: Effect[F]) {

  val dbMigration = new DbMigration()
  private val db = Transactor.fromDriverManager[F]("org.postgresql.Driver",
    configuration.database.url, configuration.database.username, configuration.database.password)
  private val helloRepository = new HelloRepository(db)
  private val hello = Hello(helloRepository)
  private val helloService = new HelloService(hello)
  private val healthService = new HealthService(List())
  private val infoService = new InfoService()

  import ServiceErrorHandling._
  val rootServices = healthService.service <+>
    infoService.service <+>
    helloService.service.withErrorHandling

  def migrateDatabase() = dbMigration.migrate(configuration)

}

object Wiring {
  def create[F[_]: Effect](): F[Wiring[F]] = {
    for {
      configuration <- readConfig
    } yield new Wiring(configuration)
  }

  private def readConfig[F[_]](implicit F: Effect[F]): F[Configuration] = pureconfig.loadConfig[Configuration].fold(
    failures => F.raiseError(new IllegalStateException(failures.toString)),
    configuration => F.delay(configuration)
  )
}
