import cats.effect.Effect
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import fs2._
import kamon.Kamon
import kamon.http4s.middleware.server.KamonSupport
import kamon.zipkin.ZipkinReporter
//import monix.execution
import org.http4s.server.blaze.BlazeBuilder

import scala.language.higherKinds

class Application[F[_]](implicit F: Effect[F])
  extends StreamApp[F]
    with StrictLogging {

  override def stream(args: List[String], requestShutdown: F[Unit]): Stream[F, StreamApp.ExitCode] = {

    Kamon.addReporter(new ZipkinReporter)

    import scala.concurrent.ExecutionContext.Implicits.global

    for {
      wiring <- Stream.eval(Wiring.create[F]())
      _ <- Stream.eval(wiring.migrateDatabase())
      server <- BlazeBuilder[F]
        .bindHttp(wiring.configuration.port, "0.0.0.0")
        .mountService(KamonSupport(wiring.rootServices), "/")
        .serve
    } yield server

  }
}
