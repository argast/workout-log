import cats.effect.Effect
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import io.circe._
import kamon.Kamon
import kamon.http4s.Log
import org.http4s.HttpService
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import health._

import scala.language.higherKinds

class HealthService[F[_]](checks: List[HealthCheck])(implicit F: Effect[F], L: Log[F])
    extends Http4sDsl[F]
    with StrictLogging {

  val service: HttpService[F] = HttpService[F] {
    case GET -> Root / "health" =>
      for {
        _ <- L.info(
          "Health check: OK")
        response <- Ok("OK")
      } yield response
  }
}
