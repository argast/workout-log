import cats.effect.Effect
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import error.AppError
import io.circe._
import kamon.Kamon
import kamon.http4s.Log
import org.http4s.{HttpService, ParseFailure, QueryParamDecoder, QueryParameterValue, Response}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import io.circe.generic.auto._
import io.circe.syntax._

import scala.language.higherKinds

class HelloService[F[_]](hello: Hello[F])(implicit F: Effect[F], L: Log[F])
    extends Http4sDsl[F]
    with StrictLogging {

  val service: HttpService[F] = HttpService[F] {
    case GET -> Root / "hello" :? params =>
      for {
        _ <- L.info(
          "Hello world1" + Kamon.currentSpan().context().traceID.string)
        message <- hello
          .helloMessage(params.get("name").flatMap(_.headOption))
        response <- Ok(message)
      } yield response

    case GET -> Root / "hellojson" =>
      for {
        message <- hello.helloMessage
        response <- Ok(Json.obj("message" -> Json.fromString(message)))
      } yield response

  }
}
