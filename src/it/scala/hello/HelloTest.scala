package hello

import cats.effect.IO
import com.softwaremill.sttp._
import com.softwaremill.sttp.asynchttpclient.cats.AsyncHttpClientCatsBackend
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

import scala.language.postfixOps

class HelloTest extends WordSpec with ScalaFutures with Matchers with TestConfig {

  implicit private val backend = AsyncHttpClientCatsBackend[IO]()

  "Hello service" should {
    "return hello" in {
      val response = sttp.get(uri"http://${testConfig.hostname}:8080/hello?name=Harry").send().unsafeRunSync()
      response.body shouldEqual Right("Hello there Harry!!!")
    }
  }
}
