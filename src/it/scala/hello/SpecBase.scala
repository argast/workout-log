package hello

import org.scalatest.{Matchers, WordSpec}
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest.time.{Millis, Seconds, Span}

trait SpecBase extends WordSpec with ScalaFutures with Matchers with PatienceConfiguration with TestConfig {

  override implicit def patienceConfig: PatienceConfig =  PatienceConfig(
    timeout = scaled(Span(20, Seconds)),
    interval = scaled(Span(5, Millis))
  )
}
