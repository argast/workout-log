package hello

import com.typesafe.config.ConfigFactory

trait TestConfig {

  case class TestConfig(hostname: String)

  protected val testConfig = pureconfig.loadConfigOrThrow[TestConfig](ConfigFactory.load(System.getProperty("test.env", "local.conf") ))

}
