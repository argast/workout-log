package hello

import cats.effect.IO
import com.softwaremill.sttp._
import com.softwaremill.sttp.asynchttpclient.cats.AsyncHttpClientCatsBackend

import scala.language.postfixOps

class InfoTest extends SpecBase {

  implicit private val backend = AsyncHttpClientCatsBackend[IO]()

  "Info page" should {
    "return successfully" in {
      val response = sttp.get(uri"http://${testConfig.hostname}:8080/info").send().unsafeRunSync()
      response.is200 shouldEqual true
    }
  }
}
