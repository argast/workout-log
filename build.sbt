val http4sVersion = "0.18.15"
val kamonVersion = "1.0.1"
val circeVersion = "0.9.3"
val doobieVersion = "0.5.3"
val sttpVersion = "1.2.3"

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning, BuildInfoPlugin, JavaServerAppPackaging, DockerPlugin, GatlingPlugin, JavaAgent)
  .configs(IntegrationTest)
  .settings(
    name := "workout-log",
    scalaVersion := "2.12.6",
    commands += Command.command("itTest") { state =>
      "dockerComposeUp" ::
        "it:test" ::
        "dockerComposeDown" ::
        state
    },
    commands += Command.command("gatlingTest") { state =>
      "dockerComposeUp" ::
        "gatling:test" ::
        "dockerComposeDown" ::
        state
    },
    resolvers ++= Seq(
      Resolver.bintrayRepo("argast","maven"),
      Resolver.sonatypeRepo("snapshots")
    ),
    javaAgents += "io.kamon" % "kamon-agent" % "0.0.8-experimental" % "runtime",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-dsl" % http4sVersion,
      "org.http4s" %% "http4s-blaze-server" % http4sVersion,
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "org.http4s" %% "http4s-circe" % http4sVersion,
      "org.typelevel" %% "cats-core" % "1.1.0",
      "org.typelevel" %% "cats-effect" % "0.10.1",
      "com.github.pureconfig" %% "pureconfig" % "0.9.1",
      "org.flywaydb" % "flyway-core" % "5.1.4",
      "org.tpolecat" %% "doobie-core"      % doobieVersion,
      "org.tpolecat" %% "doobie-postgres"  % doobieVersion,
      //"io.monix" %% "monix" % "3.0.0-RC1",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
      "io.kamon" %% "kamon-core" % "1.1.3",
      "io.kamon" %% "kamon-zipkin" % "1.0.0",
      "io.kamon" %% "kamon-scala-future" % "1.0.0",
      "io.kamon" %% "kamon-http4s" % "1.0.8",
      "io.kamon" %% "kamon-logback" % "1.0.0",
      "io.kamon" % "kamon-agent" % "0.0.8-experimental" % "runtime",
      "com.softwaremill.sttp" %% "core" % sttpVersion,
      "com.softwaremill.sttp" %% "async-http-client-backend-cats" % sttpVersion,
      "io.gatling.highcharts" % "gatling-charts-highcharts" % "3.0.0-SNAPSHOT" % "test",
      "io.gatling" % "gatling-test-framework" % "3.0.0-SNAPSHOT" % "test",
      "org.scalatest" %% "scalatest" % "3.0.5" % "test,it",
      "org.pegdown" % "pegdown" % "1.6.0" % "test,it"
    ),
    dependencyOverrides ++= Seq(
      "org.typelevel" %% "cats-effect" % "0.10.1"
    ),
    scalacOptions ++= Seq("-Ypartial-unification"),
    javaOptions := Seq("-javaagent:agent/kamon-agent.jar", "-javaagent:agent/aspectjweaver.jar")
  )
  .settings(BuildInfo.settings: _*)
  .settings(Packaging.settings: _*)
  .settings(Gatling.settings: _*)
  .settings(Versioning.settings: _*)
  .settings(DockerCompose.settings: _*)
  .settings(Testing.settings: _*)

addCompilerPlugin(
  "org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full
)

addCompilerPlugin("org.spire-math" % "kind-projector" % "0.9.7" cross CrossVersion.binary)